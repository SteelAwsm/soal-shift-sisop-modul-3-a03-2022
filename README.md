# Laporan Penjelasan dan Penyelesaian Soal

## Kelompok A03

| Nama          | NRP           |
|---------------|---------------|
|James Silaban                  | 5025201169|
|Hemakesha Ramadhani Heriqbaldi | 5025201209|
|Wina Tungmiharja               | 5025201242| 


# Soal 1

## a. Title A

**Step 1**

Fungsi Main

```
int main()
{
    int i=0;
    int err;
    char file_loc[50];
    char cwd[PATH_MAX];


    while(i<3) // loop sejumlah thread
    {
        err=pthread_create(&(tid[i]),NULL,unzipper,NULL); //membuat thread
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        else
        {
            printf("\n create thread %d success\n", i);
        }
        i++;
    }
 

    
    pthread_join(tid[0],NULL);
    pthread_join(tid[1],NULL);

       
    file_creator("music.txt"); //mbuat txt
    file_creator("quote.txt"); //mbuat txt
    
    for(int i=1; i<10; i++){
        sprintf(file_loc, "music/m%d.txt", i);
        err=pthread_create(&tid1[i-1], NULL, decode, (void*) file_loc);
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        pthread_join( tid1[i-1], NULL);
    }


    for(int i=1; i<10; i++){
        sprintf(file_loc, "quote/q%d.txt", i);
        err= pthread_create(&tid1[i-1], NULL, decode, (void*) file_loc);
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        pthread_join( tid1[i-1], NULL);
    }


    make_directory ("hasil");
    file_move("hasil/music.txt", "music.txt");
    file_move("hasil/quote.txt", "quote.txt");
    zip_hasil("hasil");
    erase_directory("hasil");
    unzip_hasil("hasil");
    file_creator("hasil/no.txt"); //mbuat txt
    delete_file("hasil.zip");
    zip_hasil("hasil");



    exit(0);

    return 0;
}
```

- penjelasan: <br />
Fungsi main dari soal1, akan dijelaskan secara bagian per bagian
- eksekusi: <br />

<br />
Fungsi Main bagian 1

```
    int i=0;
    int err;
    char file_loc[50];
    char cwd[PATH_MAX];


    while(i<3) // loop sejumlah thread
    {
        err=pthread_create(&(tid[i]),NULL,unzipper,NULL); //membuat thread
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        else
        {
            printf("\n create thread %d success\n", i);
        }
        i++;
    }
```

- penjelasan: <br />
inisialisasi variabel `i`, `err`, `file_loc[]`, `cwd[]`, dan loop sejumlah thread untuk meng-unzip file zip `music.zip` dan `quote.zip`
- eksekusi: <br />


<br />
Fungsi unzipper

```
void* unzipper(void *arg) //after all this, phase 1 is done
{
    pthread_t id=pthread_self();

    if(pthread_equal(id,tid[0])) //thread untuk bikin directory
    {
        make_directory ("music");
        make_directory ("quote");  
    }

    if(pthread_equal(id,tid[1])) //thread untuk unzip
	{ 
        int j = 0;
        while (j < 2)
        {
            pthread_t thread;
            if (j == 0)
            {
                pthread_create(&thread, NULL, unzip_music, NULL);
            }
            if (j == 0)
            {
                pthread_create(&thread, NULL, unzip_quote, NULL);
            }
            pthread_join(thread, NULL);
         j++;
        }
    }
```

- penjelasan: <br />
fungsi unzipper, digunakan untuk meng-unzip file zip `music.zip` dan `quote.zip`, dilakukan dengan pertama membuat directory `music` dan `quote` dengan menggunakan fungsi `make_directory` untuk menyimpan file-file yang ada di dalam `music.zip` dan `quote.zip` saat di unzip. lalu di dalam fungsi `unzipper`, ada thread yang digunakan untuk meng-unzip `music.zip` dan `quote.zip` secara bersamaan dengan menggunakan fungsi `unzip_music` dan `unzip_quote`.
- eksekusi: <br />


<br />
Fungsi make_directory

```
void make_directory(char *directory){// bikin directory
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
    	char *argv[] = {"mkdir", "-p", directory, NULL};
        execv("/bin/mkdir", argv);

	}
	else 
    {
    	while((wait(&statuss)) > 0);
    	return;
	}
}
```

- penjelasan: <br />
fungsi make_directory digunakan untuk membuat directory yang dibutuhkan dengan menggunakan execv `mkdir`, fungsi make_directory digunakan tiga kali di soal1, untuk membuat directory `music` dan `quote` untuk menampung file-file yang ada di dalam `music.zip` dan `quote.zip` saat di unzip, serta directory `hasil`, untuk menampung `music.txt` dan `quote.txt`
- eksekusi: <br />


<br />
Fungsi unzip_music dan unzip_quote

```
void *unzip_music(){// unzip music
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
        chdir ("/music");
    	char *argv1[] = {"unzip", fileName1, "-d", filePath1, NULL};
        execv("/usr/bin/unzip",argv1); 

	}
	else 
    {
    	while((wait(&statuss)) > 0);
	}
}

void *unzip_quote(){// unzip quote
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
        chdir ("/quote");
    	char *argv1[] = {"unzip", fileName2, "-d", filePath2, NULL};
        execv("/usr/bin/unzip",argv1); 

	}
	else 
    {
    	while((wait(&statuss)) > 0);
	}
}
```

- penjelasan: <br />
fungsi unzip_music dan unzip_quote digunakan untuk unzip `music.zip` dan `quote.zip` dengan menggunakan execv `unzip` serta mengspesifikasikan file apa yang akan di unzip dengan `fileName1` untuk file `music.zip` dan `fileName2` untuk file `quote.zip`. Setelah itu, dispesifikasikan di directory mana file-file yang telah di unzip diletakkan, dengan `filePath1` sebagai directory `music` untuk file-file `music.zip` dan `filePath2` sebagai directory `quote` untuk file-file `quote.zip`.
- eksekusi: <br />


<br />
Fungsi main bagian 2

```
    pthread_join(tid[0],NULL);
    pthread_join(tid[1],NULL);

       
    file_creator("music.txt"); //mbuat txt
    file_creator("quote.txt"); //mbuat txt
```

- penjelasan: <br />
`pthread_join` digunakan untuk memastikan thread selanjutnya akan dilaksanakan setelah thread sebelumnya selesai, setelah melakukan kedua thread, akan dibuat file `music.txt` dan `quote.txt` untuk menyimpan file-file di dalam directory `music` dan `quote` yang sudah di decode.
- eksekusi: <br />


<br />
Fungsi file_creator

```
void file_creator (char* filename)
{
    FILE *fp;
    fp  = fopen (filename, "w");
    fclose (fp);
}
```

- penjelasan: <br />
Fungsi file_creator digunakan untuk membuat file, dengan cara mem-passing nama file menggunakan `filename` dan membuat file dengan format "w" yang artinya akan dtulis.
- eksekusi: <br />



<br />
Fungsi main bagian 3

```
 for(int i=1; i<10; i++){
        sprintf(file_loc, "music/m%d.txt", i);
        err=pthread_create(&tid1[i-1], NULL, decode, (void*) file_loc);
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        pthread_join( tid1[i-1], NULL);
    }


    for(int i=1; i<10; i++){
        sprintf(file_loc, "quote/q%d.txt", i);
        err= pthread_create(&tid1[i-1], NULL, decode, (void*) file_loc);
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        pthread_join( tid1[i-1], NULL);
    }
```

- penjelasan: <br />
Fungsi main bagian 3 digunakan untuk men-decode file-file di dalam directory `music` dan `quote`, masing-masing 9 kali, karena ada 9 file di tiap directory.
- eksekusi: <br />


<br />
Fungsi decode

```
void *decode(void *file){
    char *filename, buffer[1024], *decoded, loc[20];
    filename = (char *) file;
    size_t len;
    FILE *fptr;
    fptr = fopen(filename, "r");
    fscanf(fptr, "%s", buffer);
    fclose(fptr);

    len = b64_decoded_size(buffer) + 1;
    decoded = (char*) malloc(len*sizeof(char));

    if(b64_decode(buffer, (unsigned char *) decoded, len)){
        decoded[len] = '\0';
        if(filename[0] == 'm') strcpy(loc, "music.txt");
        else if(filename[0] == 'q') strcpy(loc, "quote.txt");
        fptr = fopen(loc, "a");
        fprintf(fptr, "%s\n", decoded);
        fclose(fptr);
    }

    //free(decoded);
}
```

- penjelasan: <br />
fungsi decode digunakan untuk men-decode file-file di dalam directory `music` dan `quote`, dengan pertama membuka file dengan format "r" untuk dibaca, dan isinya yang berbentuk string akan di duplikasi kan ke dalam buffer, dimana akan di hitung panjangnya menggunakan `len = b64_decoded_size(buffer) + 1;` dan akan di decode dengan `64_decode(buffer, (unsigned char *) decoded, len`, setelah itu, string dimasukkan ke dalam `music.txt` jika nama file yang di decode memiliki huruf awal "m", yang menandakan bahwa file merupakan file music, jika huruf awal "q" yang muncul, yang menandakan bahwa file merupakan file quote, maka string akan dimasukkan ke dalam `quote.txt`.
- eksekusi: <br />
`-`


<br />
Fungsi format decode

```
size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}



int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

void b64_generate_decode_table()
{
	int    inv[80];
	size_t i;

	memset(inv, -1, sizeof(inv));
	for (i=0; i<sizeof(b64chars)-1; i++) {
		inv[b64chars[i]-43] = i;
	}
}

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}
```

- penjelasan: <br />
fungsi format decode merupakan fungsi yang berfungsi sebagai format untuk men-decode string base64. `b64_decoded_size` digunakan untuk menentukan binary data size, `b64invs` digunakan sebagai decoding table, `b64_generate_decode_table` digunakan untuk men-generate decode table, `b64_isvalidchar` digunakan untuk memvalidasi character yang ada cocok dengan Base64 character set atau tidak. `b64_decode` digunakan untuk decode base64.
- eksekusi: <br />


<br />
Fungsi main bagian 4

```
    make_directory ("hasil");
    file_move("hasil/music.txt", "music.txt");
    file_move("hasil/quote.txt", "quote.txt");
    zip_hasil("hasil");
    erase_directory("hasil");
    unzip_hasil("hasil");
    file_creator("hasil/no.txt"); //mbuat txt
    delete_file("hasil.zip");
    zip_hasil("hasil");
```

- penjelasan: <br />
`make_directory ("hasil");` digunakan untuk membuat directory hasil, yang akan menyimpan `music.txt` dan `quote.txt`. `file_move("hasil/music.txt", "music.txt");` digunakan untuk memindahkan isi `music.txt` ke dalam `hasil/music.txt`. `file_move("hasil/quote.txt", "quote.txt");` digunakan untuk memindahkan isi `quote.txt` ke dalam `hasil/quote.txt`. `zip_hasil("hasil");` digunakan untuk men-zip directory hasil.
- eksekusi: <br />


Lapres no 1 belum selesai

# Soal 2


## a. Register dan Login

```
// client
printf("Selamat datang, silahkan ketik command yand dipilih:\n");
        printf("1. registrasi\n");
        printf("2. login\n");
        printf("3. keluar\n");
        printf("command : ");
        scanf("%s", answer);
        send(sock, answer, strlen(answer), 0);

        char username[20];
        char password[20];
        char msg_reg[50];
```
```
//server
...

while ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)))
    {

        printf("Koneksi diterima\n");
        while (true)
        {
            recv(new_socket, buffer, 1024, 0);
            if (buffer[0] == 'r')
            {...}
            if (buffer[0] == 'l')
            {...}
            ...
        }
...
```

Pada awal program client , akan ditampilkan pilihan command yang dapat dipilih. Setiap pilihan yang telah dipilih user akan dikirim ke server untuk menjalankan program sesuai command yang diiunputkan.

**REGISTER**
```
// client.c
int main (){
    ...
    char username[20];
        char password[20];
        char msg_reg[50];
        if (answer[0] == 'r')
        {
            printf("Masukkan username: ");
            scanf("%s", username);
            send(sock, username, strlen(username), 0);
            memset(msg_reg, 0, sizeof(msg_reg));
            recv(sock, msg_reg, sizeof(msg_reg), 0);
            while (!strcmp(msg_reg, "gagal"))
            {
                printf("Username telah digunakan\n");
                printf("Masukkan username: ");
                scanf("%s", username);
                send(sock, username, strlen(username), 0);
                memset(msg_reg, 0, sizeof(msg_reg));
                recv(sock, msg_reg, sizeof(msg_reg), 0);
            }
            printf("Username diterima :)\n");

            printf("Masukkan password: ");
            scanf("%s", password);
            send(sock, password, strlen(password), 0);
            memset(msg_reg, 0, sizeof(msg_reg));
            recv(sock, msg_reg, sizeof(msg_reg), 0);
            while (!strcmp(msg_reg, "gagal"))
            {
                printf("Password tidak memenuhi\n");
                printf("Masukkan password: ");
                scanf("%s", password);
                send(sock, password, strlen(password), 0);
                memset(msg_reg, 0, sizeof(msg_reg));
                recv(sock, msg_reg, sizeof(msg_reg), 0);
            }
            printf("Password diterima\n");
            printf("Silahkan untuk login\n");

            sleep(2);
            system("clear");
        }
    ...
}

```
```
// server.c

...
bool checkUsername(char *userName, int sock)
{
    FILE *fuser;
    // printf("folder_user_path adalah %s\n", file_user_path);
    fuser = fopen(file_user_path, "a+");

    int counter = 0;
    while (fscanf(fuser, "%s\n", arrFile[counter]) != EOF)
        counter++;

    for (int i = 0; i < counter; i++)
    {
        if (strstr(arrFile[i], userName) != NULL)
        {
            fclose(fuser);
            char *msg_gagal = "gagal";
            send(sock, msg_gagal, strlen(msg_gagal), 0);
            return false;
        }
    }

    // fprintf(fuser, "%s\n", userName);
    char *msg_berhasil = "berhasil";
    send(sock, msg_berhasil, strlen(msg_berhasil), 0);
    fclose(fuser);
    return true;
}

bool checkPassword(char *password, int sock)
{
    int upper = 0, lower = 0, number = 0;

    int i = 0;
    while (i < strlen(password))
    {
        if (password[i] >= 48 && password[i] <= 57)
        {
            number++;
        }

        if (password[i] >= 'A' && password[i] <= 'Z')
        {
            upper++;
        }

        if (password[i] >= 'a' && password[i] <= 'z')
        {
            lower++;
        }

        i++;
    }

    if (number == 0 || upper == 0 || lower == 0)
    {
        printf("Masukkan password sesuai ketentuan\n");
        char *msg_gagal = "gagal";
        send(sock, msg_gagal, strlen(msg_gagal), 0);
        return false;
    }
    else
    {
        printf("Password diterima\n");
        char *msg_berhasil = "berhasil";
        send(sock, msg_berhasil, strlen(msg_berhasil), 0);
        return true;
    }
}

...

int main(){
    ...
    if (buffer[0] == 'r')
            {
                memset(buffer, 0, sizeof(buffer));

                char username_input[50];
                memset(username_input, 0, sizeof(username_input));
                recv(new_socket, username_input, 50, 0);
                printf("meneerima username dengan isi : %s\n", username_input);
                while (!checkUsername(username_input, new_socket))
                {
                    printf("username telah ada, coba input kembali\n");
                    memset(username_input, 0, sizeof(username_input));
                    recv(new_socket, username_input, 50, 0);
                }

                char password_input[50];
                memset(password_input, 0, sizeof(password_input));
                recv(new_socket, password_input, 50, 0);
                while (!checkPassword(password_input, new_socket))
                {
                    memset(password_input, 0, sizeof(password_input));
                    recv(new_socket, password_input, 50, 0);
                }

                FILE *fuser;
                fuser = fopen(file_user_path, "a+");
                char inputDataUser[100];
                strcpy(inputDataUser, username_input);
                strcat(inputDataUser, ":");
                strcat(inputDataUser, password_input);
                fprintf(fuser, "%s\n", inputDataUser);
                fclose(fuser);
            }
    ...
}

```
- eksekusi:
    - pada bila kita memilih register pada client.c, user akan diminta untuk input username. Kemudial client akan mengirim kepada server input username tersebut. Setelah itu, server akan mengecek username dengan syarat tidak ada username yang mirip dengan fungsi `checkUsername()`.
    - Jika memenuhi, server akan mengirimkan signal berhasil, kemudian client dapat melanjutkan input password.
    - Setelah client input password, password tersebut akan dikirim ke server, dan server akan mengecek password tersebut dengan fungsi `checkPassword()`
    - Jika password memenuhi, kemudian server akan memasukkan username dan password tersebut ke dalam file `user.txt`.

**LOGIN**
```
// client.c
int main(..){
    ...
    else if (answer[0] == 'l')
        {
            char author[50];
            printf("Masukkan username: ");
            scanf("%s", username);
            strcpy(author, username);
            printf("Masukkan password: ");
            scanf("%s", password);
            strcat(username, ":");
            printf("usernamenya adalah: %s\n", username);
            strcat(username, password);
            printf("passwordnya adalah: %s\n", password);
            send(sock, username, strlen(username), 0);
            memset(msg_reg, 0, sizeof(msg_reg));
            recv(sock, msg_reg, sizeof(msg_reg), 0);
            if (strcmp(msg_reg, "gagal")==0)
            {
                printf("User tidak dikenali. Akses ditolak.\n");
            }
            else
            {
                printf("Halo, %s\n", author);
                activity(sock, author);
            }
    ...
}
```
```
//server.c

...
bool login(char *identityInput, int sock)
{
    FILE *fuser;
    fuser = fopen(file_user_path, "r");

    int counter = 0;
    while (fscanf(fuser, "%s\n", arrFile[counter]) != EOF)
        counter++;

    for (int i = 0; i < counter; i++)
    {
        // printf("Isi arrFile ke-%d adalah %s\n", i, arrFile[i]);
        if (strcmp(arrFile[i], identityInput) == 0)
        {
            // printf("isi arrFile %d adalah %s\n", i, arrFile[i]);
            fclose(fuser);
            char *msg_berhasil = "berhasil";
            printf("berhasil\n");
            send(sock, msg_berhasil, strlen(msg_berhasil), 0);
            return true;
        }
    }


    // fprintf(fuser, "%s\n", userName);
    char *msg_gagal = "gagal";
    printf("gagal\n");
    send(sock, msg_gagal, strlen(msg_gagal), 0);
    fclose(fuser);
    return false;
}

...
int main(){
    ...
    else if (buffer[0] == 'l')
            {
                memset(buffer, 0, sizeof(buffer));
                char identityInput[100];
                memset(identityInput, 0, sizeof(identityInput));
                recv(new_socket, identityInput, 100, 0);
                printf("identity_input adalah: %s\n", identityInput);

                if (!login(identityInput, new_socket))
                {
                }
                else
                {
                    while (true)
                    {
                        char inputCommand[20];
                        memset(inputCommand, 0, sizeof(inputCommand));
                        recv(new_socket, inputCommand, 20, 0);
                        process_command(inputCommand, new_socket);
                    }
                }
            }
    ...
}
...
```
- eksekusi:
    - Jika client memilih login, maka client akan meminta input username dan password, serta mengirikannya ke server untuk dicek.
    - Setelah server menerima data, maka server akan mengecek username dan password dengan fungsi `login()` apakah username dan password terdata di `user.txt`
    - Jika benar, maka server akan mengirimkan signal berhasil ke client dan proses dapat dilanjutkan.

## b. Membuat file problem.tsv

```
// server.c
bool access_tsv(char commandtsv, char *dataToTsv, int sock)
{
    FILE *ftsv;

    if (commandtsv == 'c')
    {
        ftsv = fopen(file_tsv_path, "a+");
        fclose(ftsv);
        return true;
    }

    ...
}

...
int main(){
    ...
    access_tsv('c', "none", 0);
    ...
}
```
Membuat file problem.tsv akan dibuat saat server pertama kali dijalankan dengan memanggil fungsi `access_tsv()`.

## c. Command add

```
// client.c
...
void activity(int sock, char* author){
    while(true){
        
        printf("Pilih Command : \n");
        printf("1. add\n");
        printf("2. see\n");
        printf("3. download\n");
        printf("4. submit\n");
        printf("5. keluar\n");
        printf("command: ");
        char command[300];
        scanf("%s", command);

        if(command[0] == 'k')
            break;
        send(sock, command, strlen(command), 0);
        if(command[0] == 'a'){
            char judulProblem[50], description[200], input[200], output[200], data[700];
            printf("Judul Problem: ");
            scanf("%s", judulProblem);
            strcpy(data, judulProblem); strcat(data, ";");
            //send(sock, judulProblem, strlen(judulProblem), 0);
            printf("Filepath description: ");
            scanf("%s", description);
            strcat(data, description); strcat(data, ";");
            //send(sock, description, strlen(description), 0);
            printf("Filepath input: ");
            scanf("%s", input);
            // send(sock, input, strlen(input), 0);
            strcat(data, input); strcat(data, ";");
            printf("Filepath output.txt: ");
            scanf("%s", output);
            // send(sock, output, strlen(output), 0);
            strcat(data, output); strcat(data, ";");            
            // send(sock, author, strlen(author), 0);
            strcat(data, author);strcat(data,"\n");
            send(sock, data, strlen(data), 0);
            printf("Berhasil mengirim");
            char msg_act[20];
            recv(sock, msg_act, 20, 0);
            if(!strcmp(msg_act, "gagal")){
                printf("Data tidak dapat dimasukkan");
            }
            else{
                printf("data berhasil dimasukkan");
            }
        }
        ...
    }
    ...
}

int main(){
    ...
    activity(sock, author);
    ...
}
```
```
// server.c
...
bool access_tsv(char commandtsv, char *dataToTsv, int sock)
{
    FILE *ftsv;
    ...
    if (commandtsv == 'a')
    {
        ftsv = fopen(file_tsv_path, "a+");
        fprintf(ftsv, "%s\n", dataToTsv);
        fclose(ftsv);
        return true;
    }
    ...
}

...
bool soalC(int sock)
{
    bool returnFunction;
    char judulProblem[50]={0}, description[200]={0}, input[200]={0}, output[200]={0}, author[50]={0}, data[700];
    char pathFolderProblem[100], pathFilerDescription[100], pathFileInput[100], pathFileOutput[100];
    memset(data, 0, sizeof(data));

    recv(sock, data, sizeof(data), 0);
    printf("isi data adalah %s\n", data);
    sscanf(data, "%[^;];%[^;];%[^;];%[^;];%[^\n]\n", judulProblem, description, input, output, author);

    strcpy(pathFolderProblem, folder_path);
    strcat(pathFolderProblem, judulProblem);
    strcat(pathFolderProblem, "/");
    mkdir(pathFolderProblem, 0777);

    strcpy(pathFilerDescription, pathFolderProblem);
    strcat(pathFilerDescription, "description.txt");
    FILE *fdes;
    fdes = fopen(pathFilerDescription, "wx"); //create text for write;
    fprintf(fdes, "%s\n", description);
    fclose(fdes);

    strcpy(pathFileInput, pathFolderProblem);
    strcat(pathFileInput, "input.txt");
    FILE *fin;
    fin = fopen(pathFileInput, "wx"); //create text for write;
    fprintf(fin, "%s\n", input);
    fclose(fin);

    strcpy(pathFileOutput, pathFolderProblem);
    strcat(pathFileOutput, "output.txt");
    FILE *fout;
    fout = fopen(pathFileOutput, "wx"); //create text for write;
    fprintf(fout, "%s\n", output);
    fclose(fout);

    char datatoTsv[100];
    sprintf(datatoTsv, "%s\t%s", judulProblem, author);
    returnFunction = access_tsv('a', datatoTsv, sock);
    if (returnFunction)
    {
        printf("Data berhasil dimasukkan");
        send(sock, "berhasil", strlen("berhasil"), 0);
        return true;
    }

    printf("Data gagal dimasukkan");
    send(sock, "gagal", strlen("gagal"), 0);
    return false;
}
...
void process_command(char *command, int sock)
{
    bool returnFunction;

    if (command[0] == 'a')
    {
        returnFunction = soalC(sock);
    }
}
...
int main (){
    ...
      if (!login(identityInput, new_socket))
                {
                }
                else
                {
                    while (true)
                    {
                        char inputCommand[20];
                        memset(inputCommand, 0, sizeof(inputCommand));
                        recv(new_socket, inputCommand, 20, 0);
                        process_command(inputCommand, new_socket);
                    }
                }
    ...
}

```
- eksekusi:
    - Pada client, kita memanggil fungsi `activity()` saat client berhasil login. Fungsi `activity()` akan memberikan pilihan command yang bisa dilakukan pada database. Jika dipilih command `add`, maka, akan dimasukkan input data (judul, deskripsi, input, output), kemudian dikirim ke server, beserta authornya.
    - Server akan menjalankan fungsi `process_command()`. Process command akan mengarahkan kepada fungsi tertentu tergantung command yang dimasukkan. Jika command `add`, maka akan dipanggil fungsi `soalC`.
    - Fungsi `soalC` akan menerima data dari client. Kemudian membuat folder berdasarkan data yang diterima. Setelah itu barulah dibuat file dengan isi berasal dari data pada client.
    - Setelah itu, barulah update data pada file `problems.tsv`

## d. Command see
```
// client.c
...
void activity(int sock, char* author){
    while(true){
        ...
        else if(command[0] == 's'){
            char filetsv[100]={0}, judulProblem[50]={0}, author[50]={0};
            recv(sock, filetsv, sizeof(filetsv), 0);
            while(strcmp(filetsv, "kosong") != 0){
                sscanf(filetsv, "%s\t%s", judulProblem, author);
                printf("%s by %s\n", judulProblem,author);
                memset(filetsv, 0, sizeof(filetsv));
                recv(sock, filetsv, sizeof(filetsv), 0);
            }
            printf("selesai\n");
        }
        ...
}

...

int main(){
    ...
    activity(sock, author);
    ...
}


```

```
// server.c

...
bool access_tsv(char commandtsv, char *dataToTsv, int sock)
{
    FILE *ftsv;

    ...

    if(commandtsv == 's'){
        char judulProblem[50], author[50], filetsv[100];
        size_t len =0;
        ssize_t read;
        ftsv = fopen(file_tsv_path, "r");
        while (fscanf(ftsv, "%s\t%s", judulProblem, author) != EOF)
        {
            strcpy(filetsv, judulProblem);
            strcat(filetsv, "\t");
            strcat(filetsv, author);
            send(sock, filetsv, strlen(filetsv), 0);
            sleep(1);
        }
        
        strcpy(filetsv, "kosong");
        printf("File Telah terkirim semua\n");
        send(sock, filetsv, strlen(filetsv), 0);
        fclose(ftsv);

        return true;
    }
}
...
bool soalD(int sock){
    bool returnFunction;
    returnFunction = access_tsv('s', "none",  sock);
    return returnFunction;
}
...
void process_command(char *command, int sock)
{
    bool returnFunction;
    ...
    if (command[0] == 's'){
        returnFunction = soalD(sock);
    }
    ...
}
...

int main(){
    ...
    process_command(inputCommand, new_socket);
    ...
}

```
- eksekusi:
    - saat client memilih command `see`, maka command tersebut akan dikirim ke server, kemudian server akan menjalankan fungsi sesuai dengan command
    - Dijalankan fungsi `soalD()`, dimana fungsi tersebut akan memanggil fungsi `access_tsv()`.
    - Pada fungsi `access_tsv()`, akan dilakukan pembacaan file `problem.tsv`. Kemudian setiap data pada file tersebut akan dikirimkan ke client.
    - Client akan menerima inputan isi file `problem.tsv`, kemudian akan ditampilkan dengan ketentuan yang ada.

Dokumentasi dapat dilihat pada Link berikut:

https://drive.google.com/drive/folders/1mP9ieSkCvN9c6ndW1jdHNC30m6aiMgcx?usp=sharing

# Soal 3

## a. mengextract zip yang diberikan ke dalam folder “/home/[user]/shift3/”

bagian ini dilakukan secara manual karena pelarangan menggunkaana fungsi unzip

## b.c.d mengkategorikan seluruh file pada working directory secara rekursif

pertama kita akan melakukan inisiasi working directorry dulu dan kemudian diikutin edengan melakukan pengkatoregiran file
```
char folder_directory[100] = "hartakarun";
char cwd[PATH_MAX];
if (getcwd(cwd, sizeof(cwd)) != NULL)
{
  strcat(cwd, "/");
  printf("%s", cwd);
  strcat(cwd, folder_directory);
  //membaca working direcotry dan melakukan kategorisasi
  get_file(cwd);
}
```

```
/**
 * fungsi untuk melakukan pembacaan pada direktori /hartakarun secara rekursif
 * dan membuat thread untuk setiap file dan memindahkan file tersebut dengan fungsi
 * move_file()
 * @param working_directory = direktori untuk dibaca yaitu [root]/hartakarun
 */
void get_file(char *working_directory)
{
    char path[1000];
    struct dirent *en_a;
    struct stat buffer;
    DIR *directory_a = opendir(working_directory);

    if (!directory_a) return;

    while ((en_a = readdir(directory_a)) != NULL)
    {
        if(strcmp(en_a->d_name, ".") != 0 && strcmp(en_a->d_name, "..") != 0)
        {
            snprintf(path, 1000, "%s/%s", working_directory, en_a->d_name); 
            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                pthread_t thread;
                pthread_create(&thread, NULL, move_file, (void *)path);
                pthread_join(thread, NULL);
            }
            get_file(path);
        }
    }
    closedir(directory_a);
}

```

```
/**
 * fungsi untuk memindahkan file sesuai dengan ekstensi file tersebut
 * @param file_directory : direktori file yang ingin dicek
 */
void *move_file(void *file_directory)
{
    char cwd[PATH_MAX];
    strcat(cwd, "/hartakarun");

    char *file_name = strrchr(file_directory, '/');
    
    char new_directory[1000], new_full_directory[1000], file[1000];
    int i;
    
    if (strrchr(file_directory, '/')[1] == '.'){
        //khusus untuk file tersembunyi
        strcpy(new_directory, "Hidden");
    }
    else if (strstr(file_directory, ".") != NULL){
        //khusus untuk file dengan ekstensi
        strcpy(file, file_directory);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        for (i=0;token[i];i++)
        {
            token[i]=tolower(token[i]);
        }
        strcpy(new_directory, token);
    }
    else {
        //khusus untuk file tanpa ekstensi
        strcpy(new_directory, "Unknown");
    }

    //mengecek apakah file ada
    int exist = isFileExist(file_directory);
    if (exist){
        //membuat direktori baru
        char base_directory[1000];
        snprintf(base_directory, 1000, "%s/%s",folder_directory, new_directory); 
        mkdir(base_directory, 0755);
    }
    if (getcwd(cwd, sizeof(cwd)) != NULL){
        //memindahkan file sesuai dengan direktorinya
        snprintf(new_full_directory, 1000, "%s/%s/%s%s", cwd, folder_directory, new_directory, file_name); 
        rename(file_directory, new_full_directory);
    }
}

```

'''
/**
 * mengecek apakah sebuah file ada dalam direktoti atau tidak
 * @param file_name
 */
int isFileExist(const char *file_name)
{
    struct stat buffer;
    int exist = stat(file_name, &buffer);
    if (exist == 0)
        return 1;
    else
        return 0;
}
'''

## d. di-zip terlebih dahulu dengan nama “hartakarun.zip” ke working directory dari program client













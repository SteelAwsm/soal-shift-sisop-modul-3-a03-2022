#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <errno.h>
#include <dirent.h>
#include <sys/socket.h>
#include <limits.h>
#include <libgen.h>
#include <netinet/in.h>
#include <wait.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 1024

char folder_directory[100] = "hartakarun";


int send_file(int socket, char *file_name){
  char buffer[BUFFER_SIZE] = {0};
  char fpath[BUFFER_SIZE];
  strcpy(fpath, "./");
  strcat(fpath, file_name);
  FILE *file = fopen(fpath, "r");
  if (file == NULL)
  {
    printf("File %s not found.\n", file_name);
    return -1;
  }
  bzero(buffer, BUFFER_SIZE);
  int file_size;
  while ((file_size = fread(buffer, sizeof(char), BUFFER_SIZE, file)) > 0)
  {
    if (send(socket, buffer, file_size, 0) < 0)
    {
      fprintf(stderr, "Failed to send file %s.\n", file_name);
      break;
    }
    bzero(buffer, BUFFER_SIZE);
  }
  fclose(file);
  return 0;
}

int make_connection(char *file_name){
    //inisiasi client
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char *hello = "Request dari Client";
    char buffer[BUFFER_SIZE] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    //mengirimkan data
    send(sock , hello , strlen(hello) , 0 );
    send_file(sock, file_name);
    printf("selesai mengirim file\n");
    return 0;
}

void zip_and_send_file(){
    pid_t child_id;
    int status;

    child_id = fork();

    if (child_id < 0) {
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }

    if (child_id == 0) {
        // this is child

        char *argv[]={"zip","-mr","hartakarun.zip","/home/wina/Documents/soal-shift-sisop-modul-3-a03-2022/soal3/hartakarun", NULL};
        execv("/usr/bin/zip",argv);
    } else {
        // this is parent
        while ((wait(&status)) > 0);
        printf("Mengirim file");
        make_connection("hartakarun.zip");
    }
    
}

int main(int argc, char const *argv[])
{   
    //melakukan zip dan mengirimakn file ke server
    zip_and_send_file();
    return 0;
}


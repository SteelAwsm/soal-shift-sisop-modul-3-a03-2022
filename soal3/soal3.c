#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <errno.h>
#include <dirent.h>
#include <limits.h>
#include <libgen.h>
#include <wait.h>

#define BUFFER_SIZE 1024

char folder_directory[100] = "hartakarun";

/**
 * mengecek apakah sebuah file ada dalam direktoti atau tidak
 * @param file_name
 */
int isFileExist(const char *file_name)
{
    struct stat buffer;
    int exist = stat(file_name, &buffer);
    if (exist == 0)
        return 1;
    else
        return 0;
}

/**
 * fungsi untuk memindahkan file sesuai dengan ekstensi file tersebut
 * @param file_directory : direktori file yang ingin dicek
 */
void *move_file(void *file_directory)
{
    char cwd[PATH_MAX];
    strcat(cwd, "/hartakarun");

    char *file_name = strrchr(file_directory, '/');
    
    char new_directory[1000], new_full_directory[1000], file[1000];
    int i;
    
    if (strrchr(file_directory, '/')[1] == '.'){
        //khusus untuk file tersembunyi
        strcpy(new_directory, "Hidden");
    }
    else if (strstr(file_directory, ".") != NULL){
        //khusus untuk file dengan ekstensi
        strcpy(file, file_directory);
        strtok(file, ".");
        char *token = strtok(NULL, "");
        for (i=0;token[i];i++)
        {
            token[i]=tolower(token[i]);
        }
        strcpy(new_directory, token);
    }
    else {
        //khusus untuk file tanpa ekstensi
        strcpy(new_directory, "Unknown");
    }

    //mengecek apakah file ada
    int exist = isFileExist(file_directory);
    if (exist){
        //membuat direktori baru
        char base_directory[1000];
        snprintf(base_directory, 1000, "%s/%s",folder_directory, new_directory); 
        mkdir(base_directory, 0755);
    }
    if (getcwd(cwd, sizeof(cwd)) != NULL){
        //memindahkan file sesuai dengan direktorinya
        snprintf(new_full_directory, 1000, "%s/%s/%s%s", cwd, folder_directory, new_directory, file_name); 
        rename(file_directory, new_full_directory);
    }
}

/**
 * fungsi untuk melakukan pembacaan pada direktori /hartakarun secara rekursif
 * dan membuat thread untuk setiap file dan memindahkan file tersebut dengan fungsi
 * move_file()
 * @param working_directory = direktori untuk dibaca yaitu [root]/hartakarun
 */
void get_file(char *working_directory)
{
    char path[1000];
    struct dirent *en_a;
    struct stat buffer;
    DIR *directory_a = opendir(working_directory);

    if (!directory_a) return;

    while ((en_a = readdir(directory_a)) != NULL)
    {
        if(strcmp(en_a->d_name, ".") != 0 && strcmp(en_a->d_name, "..") != 0)
        {
            snprintf(path, 1000, "%s/%s", working_directory, en_a->d_name); 
            if (stat(path, &buffer) == 0 && S_ISREG(buffer.st_mode))
            {
                pthread_t thread;
                pthread_create(&thread, NULL, move_file, (void *)path);
                pthread_join(thread, NULL);
            }
            get_file(path);
        }
    }
    closedir(directory_a);
}

int main(int argc, char const *argv[])
{   
    //inisiasi working directory
    char cwd[PATH_MAX];
    if (getcwd(cwd, sizeof(cwd)) != NULL)
    {
        strcat(cwd, "/");
        printf("%s", cwd);
        strcat(cwd, folder_directory);
        //membaca working direcotry dan melakukan kategorisasi
        get_file(cwd);
    }
    return 0;
}


#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdbool.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <libgen.h>


#define PORT 8080
#define BUFFER_SIZE 1024

/**
 * mengecek apakah sebuah file ada dalam direktoti atau tidak
 * @param file_name
 */
int isFileExist(const char *file_name)
{
    struct stat buffer;
    int exist = stat(file_name, &buffer);
    if (exist == 0)
        return 1;
    else
        return 0;
}

/**
 * 
 * @param socket : socket dari client
 * @param file_name 
 */
int download_file(int socket, char *file_name)
{
    char buffer[BUFFER_SIZE] = {0};
    char fpath[BUFFER_SIZE];

    strcpy(fpath, "./");
    strcat(fpath, file_name);

    FILE *read_file = fopen(fpath, "wb");

    if (read_file == NULL)
    {
        printf("File %s, tidak dapat disimpan di server\n", file_name);
    }
    else
    {
        printf("Menerima file %s dari client...\n", file_name);
        bzero(buffer, BUFFER_SIZE);
        int file_size = 0;

        //menerima file dari client
        while ((file_size = recv(socket, buffer, BUFFER_SIZE, 0)) > 0)
        {
                printf("Mulai\n");

            int write_size = fwrite(buffer, sizeof(char), file_size, read_file);
            if (write_size < file_size)
            {
                printf("Gagal menyimpan file di server\n");
                return 0;
            }
            bzero(buffer, BUFFER_SIZE);
            if (file_size == 0 || file_size != BUFFER_SIZE)
            {
                
                break;
            }
        }
        printf("File telah disimpan\n");
        
    }
    fclose(read_file);
}

int main(int argc, char const *argv[]) {

    //inisiasi koneksi socket
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[BUFFER_SIZE] = {0};
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
    
    valread = read( new_socket , buffer, BUFFER_SIZE);

    printf("Mendengarkan ... \n");

    //melakukan download file dari client
    download_file(new_socket, "hartakarun.zip");
    close(new_socket);
    return 0;
}
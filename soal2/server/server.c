#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#define PORT 8080

typedef struct isiFile
{
    char description[200];
    char input[200];
    char output[200];
} isiFile;

char folder_path[] = "/home/james/Desktop/seslab/pert3/soal2/server/";
char file_user_path[100];
char file_tsv_path[100];
char arrFile[100][50];
isiFile fileContent;

bool checkUsername(char *userName, int sock)
{
    FILE *fuser;
    // printf("folder_user_path adalah %s\n", file_user_path);
    fuser = fopen(file_user_path, "a+");

    int counter = 0;
    while (fscanf(fuser, "%s\n", arrFile[counter]) != EOF)
        counter++;

    for (int i = 0; i < counter; i++)
    {
        if (strstr(arrFile[i], userName) != NULL)
        {
            fclose(fuser);
            char *msg_gagal = "gagal";
            send(sock, msg_gagal, strlen(msg_gagal), 0);
            return false;
        }
    }

    // fprintf(fuser, "%s\n", userName);
    char *msg_berhasil = "berhasil";
    send(sock, msg_berhasil, strlen(msg_berhasil), 0);
    fclose(fuser);
    return true;
}

bool checkPassword(char *password, int sock)
{
    int upper = 0, lower = 0, number = 0;

    int i = 0;
    while (i < strlen(password))
    {
        if (password[i] >= 48 && password[i] <= 57)
        {
            number++;
        }

        if (password[i] >= 'A' && password[i] <= 'Z')
        {
            upper++;
        }

        if (password[i] >= 'a' && password[i] <= 'z')
        {
            lower++;
        }

        i++;
    }

    if (number == 0 || upper == 0 || lower == 0)
    {
        printf("Masukkan password sesuai ketentuan\n");
        char *msg_gagal = "gagal";
        send(sock, msg_gagal, strlen(msg_gagal), 0);
        return false;
    }
    else
    {
        printf("Password diterima\n");
        char *msg_berhasil = "berhasil";
        send(sock, msg_berhasil, strlen(msg_berhasil), 0);
        return true;
    }
}

bool login(char *identityInput, int sock)
{
    FILE *fuser;
    fuser = fopen(file_user_path, "r");

    int counter = 0;
    while (fscanf(fuser, "%s\n", arrFile[counter]) != EOF)
        counter++;

    for (int i = 0; i < counter; i++)
    {
        printf("Isi arrFile ke-%d adalah %s\n", i, arrFile[i]);
        if (strcmp(arrFile[i], identityInput) == 0)
        {
            printf("isi arrFile %d adalah %s\n", i, arrFile[i]);
            fclose(fuser);
            char *msg_berhasil = "berhasil";
            printf("berhasil\n");
            send(sock, msg_berhasil, strlen(msg_berhasil), 0);
            return true;
        }
    }


    // fprintf(fuser, "%s\n", userName);
    char *msg_gagal = "gagal";
    printf("gagal\n");
    send(sock, msg_gagal, strlen(msg_gagal), 0);
    fclose(fuser);
    return false;
}

bool access_tsv(char commandtsv, char *dataToTsv, int sock)
{
    FILE *ftsv;

    if (commandtsv == 'c')
    {
        ftsv = fopen(file_tsv_path, "a+");
        fclose(ftsv);
        return true;
    }

    if (commandtsv == 'a')
    {
        ftsv = fopen(file_tsv_path, "a+");
        fprintf(ftsv, "%s\n", dataToTsv);
        fclose(ftsv);
        return true;
    }

    if(commandtsv == 's'){
        char judulProblem[50], author[50], filetsv[100];
        size_t len =0;
        ssize_t read;
        ftsv = fopen(file_tsv_path, "r");
        while (fscanf(ftsv, "%s\t%s", judulProblem, author) != EOF)
        {
            strcpy(filetsv, judulProblem);
            strcat(filetsv, "\t");
            strcat(filetsv, author);
            send(sock, filetsv, strlen(filetsv), 0);
            sleep(1);
        }
        
        strcpy(filetsv, "kosong");
        printf("File Telah terkirim semua\n");
        send(sock, filetsv, strlen(filetsv), 0);
        fclose(ftsv);

        return true;
    }


}

bool soalC(int sock)
{
    bool returnFunction;
    char judulProblem[50]={0}, description[200]={0}, input[200]={0}, output[200]={0}, author[50]={0}, data[700];
    char pathFolderProblem[100], pathFilerDescription[100], pathFileInput[100], pathFileOutput[100];
    memset(data, 0, sizeof(data));

    recv(sock, data, sizeof(data), 0);
    printf("isi data adalah %s\n", data);
    sscanf(data, "%[^;];%[^;];%[^;];%[^;];%[^\n]\n", judulProblem, description, input, output, author);

    strcpy(pathFolderProblem, folder_path);
    strcat(pathFolderProblem, judulProblem);
    strcat(pathFolderProblem, "/");
    mkdir(pathFolderProblem, 0777);

    strcpy(pathFilerDescription, pathFolderProblem);
    strcat(pathFilerDescription, "description.txt");
    FILE *fdes;
    fdes = fopen(pathFilerDescription, "wx"); //create text for write;
    fprintf(fdes, "%s\n", description);
    fclose(fdes);

    strcpy(pathFileInput, pathFolderProblem);
    strcat(pathFileInput, "input.txt");
    FILE *fin;
    fin = fopen(pathFileInput, "wx"); //create text for write;
    fprintf(fin, "%s\n", input);
    fclose(fin);

    strcpy(pathFileOutput, pathFolderProblem);
    strcat(pathFileOutput, "output.txt");
    FILE *fout;
    fout = fopen(pathFileOutput, "wx"); //create text for write;
    fprintf(fout, "%s\n", output);
    fclose(fout);

    char datatoTsv[100];
    sprintf(datatoTsv, "%s\t%s", judulProblem, author);
    returnFunction = access_tsv('a', datatoTsv, sock);
    if (returnFunction)
    {
        printf("Data berhasil dimasukkan");
        send(sock, "berhasil", strlen("berhasil"), 0);
        return true;
    }

    printf("Data gagal dimasukkan");
    send(sock, "gagal", strlen("gagal"), 0);
    return false;
}

bool soalD(int sock){
    bool returnFunction;
    returnFunction = access_tsv('s', "none",  sock);
    return returnFunction;
}

// bool soalG(int sock, char* acceptFile){
//     bool returnFunction;
//     char pathFileProblem[100], judul[50], output[200];
//     sscanf(acceptFile, "%[^;]s;%s", judul, output);
//     printf("judulnya adalah %s\n", judul);
//     printf("outputnya adalah %s\n", output);
//     strcpy(pathFileProblem, folder_path);
//     strcat(pathFileProblem, judul);
//     strcat(pathFileProblem, "/");
//     strcat(pathFileProblem, "output/");

//     FILE* foutput;
//     char ch;
//     int counter=0;
//     foutput = fopen(pathFileProblem, "r");
//     while (!feof(foutput))
//     {
//         ch = fgetc(foutput);
//         if(ch != output[counter]){
//             char msg_sub[10]={"salah"};
//             printf("wa\n");
//             send(sock, msg_sub, strlen(msg_sub), 0);
//             fclose(foutput);
//             return false;
//         }
//     }
//     char msg_sub[10]={"benar"};
//     printf("AC\n");
//     send(sock, msg_sub, strlen(msg_sub), 0);
//     fclose(foutput);
//     return true;

// }

void process_command(char *command, int sock)
{
    bool returnFunction;

    if (command[0] == 'a')
    {
        returnFunction = soalC(sock);
    }

    if (command[0] == 's' && command[1] == 'e'){
        returnFunction = soalD(sock);
    }
    
    if (command[0] == 's' && command[1] == 'u'){
        char acceptFile[150];
        recv(sock, acceptFile, sizeof(acceptFile), 0);
        returnFunction = soalG(sock, acceptFile);
    }
}

int main()
{
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    strcpy(file_user_path, folder_path);
    strcat(file_user_path, "user.txt");
    strcpy(file_tsv_path, folder_path);
    strcat(file_tsv_path, "problems.tsv");
    access_tsv('c', "none", 0);

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("Menyambungkan ...\n");
    while ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)))
    {

        printf("Koneksi diterima\n");
        while (true)
        {
            recv(new_socket, buffer, 1024, 0);
            if (buffer[0] == 'r')
            {
                memset(buffer, 0, sizeof(buffer));

                char username_input[50];
                memset(username_input, 0, sizeof(username_input));
                recv(new_socket, username_input, 50, 0);
                printf("meneerima username dengan isi : %s\n", username_input);
                while (!checkUsername(username_input, new_socket))
                {
                    printf("username telah ada, coba input kembali\n");
                    memset(username_input, 0, sizeof(username_input));
                    recv(new_socket, username_input, 50, 0);
                }

                char password_input[50];
                memset(password_input, 0, sizeof(password_input));
                recv(new_socket, password_input, 50, 0);
                while (!checkPassword(password_input, new_socket))
                {
                    memset(password_input, 0, sizeof(password_input));
                    recv(new_socket, password_input, 50, 0);
                }

                FILE *fuser;
                fuser = fopen(file_user_path, "a+");
                char inputDataUser[100];
                strcpy(inputDataUser, username_input);
                strcat(inputDataUser, ":");
                strcat(inputDataUser, password_input);
                fprintf(fuser, "%s\n", inputDataUser);
                fclose(fuser);
            }
            else if (buffer[0] == 'l')
            {
                memset(buffer, 0, sizeof(buffer));
                char identityInput[100];
                memset(identityInput, 0, sizeof(identityInput));
                recv(new_socket, identityInput, 100, 0);
                printf("identity_input adalah: %s\n", identityInput);

                if (!login(identityInput, new_socket))
                {
                }
                else
                {
                    while (true)
                    {
                        char inputCommand[20];
                        memset(inputCommand, 0, sizeof(inputCommand));
                        recv(new_socket, inputCommand, 20, 0);
                        process_command(inputCommand, new_socket);
                    }
                }
            }
            else
            {
                break;
            }
        }

        close(new_socket);
    }

    printf("terlewati\n");

    return 0;
}
#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include <math.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<wait.h>
#include<dirent.h> 


char filePath1[200] = "/home/hemakesha/Documents/soal-shift-sisop-modul-3-a03-2022/soal1/quote"; 
char fileName1[200] = "/home/hemakesha/Documents/soal-shift-sisop-modul-3-a03-2022/soal1/quote.zip";

char filePath2[200] = "/home/hemakesha/Documents/soal-shift-sisop-modul-3-a03-2022/soal1/music";
char fileName2[200] = "/home/hemakesha/Documents/soal-shift-sisop-modul-3-a03-2022/soal1/music.zip";
    


pthread_t tid[3]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;

pthread_t tid1[9]; //second array untuk second thread
pid_t child1;

const char b64chars[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"; //character untuk b64
void make_directory(char *directory){// bikin directory
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
    	char *argv[] = {"mkdir", "-p", directory, NULL};
        execv("/bin/mkdir", argv);

	}
	else 
    {
    	while((wait(&statuss)) > 0);
    	return;
	}
}

void erase_directory(char *directory){// erase directory
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
    	char *argv[] = {"rm", "-r", directory, NULL};
        execv("/bin/rm", argv);

	}
	else 
    {
    	while((wait(&statuss)) > 0);
    	return;
	}
}

void delete_file(char *file){
  pid_t child_id = fork();
	int status;

	if (child_id < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id == 0) {
    	char *argv[] = {"rm",file, NULL};
    	execv("/bin/rm", argv);
	}
	else 
    {
    	while((wait(&status)) > 0);
    	return;
	}
}


void *unzip_music(){// unzip music
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
        chdir ("/music");
    	char *argv1[] = {"unzip", fileName1, "-d", filePath1, NULL};
        execv("/usr/bin/unzip",argv1); 

	}
	else 
    {
    	while((wait(&statuss)) > 0);
	}
}

void *unzip_quote(){// unzip quote
    pid_t child_id_m = fork();
	int statuss;

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
        chdir ("/quote");
    	char *argv1[] = {"unzip", fileName2, "-d", filePath2, NULL};
        execv("/usr/bin/unzip",argv1); 

	}
	else 
    {
    	while((wait(&statuss)) > 0);
	}
}
void* unzipper(void *arg) //after all this, phase 1 is done
{
    pthread_t id=pthread_self();

    if(pthread_equal(id,tid[0])) //thread untuk bikin directory
    {
        make_directory ("music");
        make_directory ("quote");  
    }

    if(pthread_equal(id,tid[1])) //thread untuk unzip
	{ 
        int j = 0;
        while (j < 2)
        {
            pthread_t thread;
            if (j == 0)
            {
                pthread_create(&thread, NULL, unzip_music, NULL);
            }
            if (j == 0)
            {
                pthread_create(&thread, NULL, unzip_quote, NULL);
            }
            pthread_join(thread, NULL);
         j++;
        }
    }


    return NULL;

}

void file_creator (char* filename)
{
    FILE *fp;
    fp  = fopen (filename, "w");
    fclose (fp);
}

void file_move(char* dest, char* src){
    FILE *fptr1, *fptr2;
    char temp;
    fptr1 = fopen(src, "r");
    fptr2 = fopen(dest, "w");
    while(fscanf(fptr1, "%c", &temp) != EOF) fprintf(fptr2, "%c", temp);
    fclose(fptr1);
    fclose(fptr2);
    remove(src);
}

void *zip_hasil(char *dir){//zip hasil
    pid_t child_id_m = fork();
	int statusss;

    char zip[20];
    sprintf(zip, "%s.zip", dir);

    if (child_id_m < 0) {
        exit(EXIT_FAILURE);
    }

    if(child_id_m == 0){
        char* argv[] = {"zip", "-P","mihinomenesthemakesha", "-r", zip, dir, NULL};
        execv("/bin/zip",argv);
        exit(0);
    }
    else while ((wait(&statusss)) > 0);
}

void *unzip_hasil(char *dir){// unzip hasil
    pid_t child_id_m = fork();
	int statussss;

    char adr[20];
    sprintf(adr, "%s.zip", dir);

	if (child_id_m < 0) 
    {
    	exit(EXIT_FAILURE);
	}
	if (child_id_m == 0) 
    {
        char* argv[] = {"unzip", "-P", "mihinomenesthemakesha", adr, NULL};
        execv("/bin/unzip",argv);
        exit(0);
	}
	else 
    {
    	while((wait(&statussss)) > 0);
	}
}




size_t b64_decoded_size(const char *in)
{
	size_t len;
	size_t ret;
	size_t i;

	if (in == NULL)
		return 0;

	len = strlen(in);
	ret = len / 4 * 3;

	for (i=len; i-->0; ) {
		if (in[i] == '=') {
			ret--;
		} else {
			break;
		}
	}

	return ret;
}



int b64invs[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

void b64_generate_decode_table()
{
	int    inv[80];
	size_t i;

	memset(inv, -1, sizeof(inv));
	for (i=0; i<sizeof(b64chars)-1; i++) {
		inv[b64chars[i]-43] = i;
	}
}

int b64_isvalidchar(char c)
{
	if (c >= '0' && c <= '9')
		return 1;
	if (c >= 'A' && c <= 'Z')
		return 1;
	if (c >= 'a' && c <= 'z')
		return 1;
	if (c == '+' || c == '/' || c == '=')
		return 1;
	return 0;
}

int b64_decode(const char *in, unsigned char *out, size_t outlen)
{
	size_t len;
	size_t i;
	size_t j;
	int    v;

	if (in == NULL || out == NULL)
		return 0;

	len = strlen(in);
	if (outlen < b64_decoded_size(in) || len % 4 != 0)
		return 0;

	for (i=0; i<len; i++) {
		if (!b64_isvalidchar(in[i])) {
			return 0;
		}
	}

	for (i=0, j=0; i<len; i+=4, j+=3) {
		v = b64invs[in[i]-43];
		v = (v << 6) | b64invs[in[i+1]-43];
		v = in[i+2]=='=' ? v << 6 : (v << 6) | b64invs[in[i+2]-43];
		v = in[i+3]=='=' ? v << 6 : (v << 6) | b64invs[in[i+3]-43];

		out[j] = (v >> 16) & 0xFF;
		if (in[i+2] != '=')
			out[j+1] = (v >> 8) & 0xFF;
		if (in[i+3] != '=')
			out[j+2] = v & 0xFF;
	}

	return 1;
}

void *decode(void *file){
    char *filename, buffer[1024], *decoded, loc[20];
    filename = (char *) file;
    size_t len;
    FILE *fptr;
    fptr = fopen(filename, "r");
    fscanf(fptr, "%s", buffer);
    fclose(fptr);

    len = b64_decoded_size(buffer) + 1;
    decoded = (char*) malloc(len*sizeof(char));

    if(b64_decode(buffer, (unsigned char *) decoded, len)){
        decoded[len] = '\0';
        if(filename[0] == 'm') strcpy(loc, "music.txt");
        else if(filename[0] == 'q') strcpy(loc, "quote.txt");
        fptr = fopen(loc, "a");
        fprintf(fptr, "%s\n", decoded);
        fclose(fptr);
    }

    //free(decoded);
}




int inv[80];

int main()
{
    int i=0;
    int err;
    char file_loc[50];
    char cwd[PATH_MAX];


    while(i<3) // loop sejumlah thread
    {
        err=pthread_create(&(tid[i]),NULL,unzipper,NULL); //membuat thread
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        else
        {
            printf("\n create thread %d success\n", i);
        }
        i++;
    }
 

    
    pthread_join(tid[0],NULL);
    pthread_join(tid[1],NULL);

       
    file_creator("music.txt"); //mbuat txt
    file_creator("quote.txt"); //mbuat txt
    
    for(int i=1; i<10; i++){
        sprintf(file_loc, "music/m%d.txt", i);
        err=pthread_create(&tid1[i-1], NULL, decode, (void*) file_loc);
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        pthread_join( tid1[i-1], NULL);
    }


    for(int i=1; i<10; i++){
        sprintf(file_loc, "quote/q%d.txt", i);
        err= pthread_create(&tid1[i-1], NULL, decode, (void*) file_loc);
        if(err!=0) //cek error
        {
            printf("\n can't create thread : [%s]",strerror(err));
        }
        pthread_join( tid1[i-1], NULL);
    }


    make_directory ("hasil");
    file_move("hasil/music.txt", "music.txt");
    file_move("hasil/quote.txt", "quote.txt");
    zip_hasil("hasil");
    erase_directory("hasil");
    unzip_hasil("hasil");
    file_creator("hasil/no.txt"); //mbuat txt
    delete_file("hasil.zip");
    zip_hasil("hasil");



    exit(0);

    return 0;
}
  